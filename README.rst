========================================
Akamai SecureHD Token Authorization VMOD
========================================

**THIS BRANCH IS FOR VARNISH-CACHE MASTER trunk >7.3 ONLY**

See branches for support of older versions.

.. role:: ref(emphasis)

.. _Varnish-Cache: https://varnish-cache.org/

This Varnish Module (VMOD) supports use of the SecureHD Policy service
provided by Akamai Media Services.

PROJECT RESOURCES
=================

* The primary repository is at https://code.uplex.de/uplex-varnish/libvmod-hoailona

  This server does not accept user registrations, so please use ...

* the mirror at https://gitlab.com/uplex/varnish/libvmod-hoailona for issues,
  merge requests and all other interactions.

DESCRIPTION
===========

The full documentation of the VMOD is in :ref:`vmod_hoailona(3)`. If
you are reading this document online, it should be available as
`vmod_hoailona.man.rst <src/vmod_hoailona.man.rst>`_.

This Varnish Module (VMOD) supports use of the SecureHD Policy service
provided by Akamai Media Services. Applications of the VMOD include:

* Defining policies for access to media content:

  * Policy type TOKEN: token authorization required, with a TTL
    (time-to-live) limiting the duration of authorized access, and
    possibly with a shared secret used for keyed-hash message
    authentication codes (HMACs) that are required for authorization

  * Policy type OPEN: access permitted without authorization

  * Policy type DENY: access denied

* Assigning policies to hosts, either globally for a host, or for
  sets of paths defined for the host

* Determining which policy holds for a given host and path

* Generating authorization tokens

This manual presupposes familiarity with the Akamai SecureHD
service. For more information, see the documentation provided by
Akamai (see `Akamai documentation`_).

The VMOD does not provide cryptographic code to generate HMACs, but it
does provide the means to associate shared secrets with a policy,
which can be used together with a VMOD that does compute HMACs (such
as the ``blobdigest`` VMOD, see `SEE ALSO`_).

The name of the VMOD is inspired by the Hawaiian word *ho`ailona*, for
"sign" or "symbol" (pronounced "ho-eye-lona"), which we believe to be
a suitable translation for "token". We welcome feedback from speakers
of Hawaiian about the choice of the name.

Defining policies
-----------------

Policies are defined by means of ``policy`` objects that are
constructed in ``vcl_init``. A policy is defined by its type (TOKEN,
OPEN or DENY), a TTL for the TOKEN type, and possibly a shared secret
used for authorization. For example::

  import hoailona;
  import blob;

  sub vcl_init {
      # Define a policy for token authorization lasting one hour,
      # and associate it with a shared secret.
      new token_policy
	  = hoailona.policy(TOKEN, 1h,
			    blob.decode(encoded="secret"));

      # Define a policy for open access (authorization not required)
      new open_policy = hoailona.policy(OPEN);

      # Define an "access denied" policy
      new deny_policy = hoailona.policy(DENY);
  }

Policy objects have no methods; they become useful when they are
assigned to hosts and paths, as shown in the following.

Assigning policies to hosts and paths
-------------------------------------

Most of the work of the VMOD is done through the ``hosts`` object,
which is used in ``vcl_init`` to assign policies to hostnames, either
globally for a host, or for sets of paths on a host.  Patterns for
paths are defined with the same syntax used by Akamai's SecureHD
Policy Editor::

  sub vcl_init {
      # After policies have been defined as shown above ...
      new config = hoailona.hosts();

      # Assign the token_policy globally to host example.com
      config.add("example.com", "token_policy");

      # Assign the open_policy to the path /foo/bar on host example.org
      config.add("example.org", "open_policy", "/foo/bar");

      # Assign the deny_policy to any path beginning with /baz/quux
      # on subdomains of example.org
      config.add("*.example.org", "deny_policy", "/baz/quux/...");
  }

Policies are assigned by using strings that must exactly match the
object names (the symbols in the VCL source) for policy objects that
were previously defined in ``vcl_init``. Details about permissible
host names and the pattern syntax used for paths are given below.

Determining the policy for a host and path
------------------------------------------

After policies and their assignments to hosts and paths have been
configured in ``vcl_init``, the policy that holds for a given host and
path can be determined from the ``policy`` method of the ``hosts``
object::

  sub vcl_recv {
      # The policy method returns 0 for policy type DENY
      if (config.policy(req.http.Host, req.url) == 0) {
	 # Handle "access denied" by returning 403 Forbidden
	 return(synth(403));
      }

      # .policy() returns 1 for policy type OPEN
      if (config.policy() == 1) {
	 return(pass);
      }

      # .policy() returns 2 for policy type TOKEN
      if (config.policy() == 2) {
	 # Handle token authorization ...
	 # [...]
      }
  }

Generating authorization tokens
-------------------------------

When the policy type TOKEN has been determined for a host and path
(return value 2 from the ``.policy()`` method), the ``.token()``
method can be used to generate the non-cryptographic portion of
the authorization token, and ``.secret()`` can be used to retrieve
the shared secret associated with the policy, to generate the
HMAC for the token::

  import blobdigest;
  import blob;

  sub vcl_recv {
      # .policy() returns 2 for policy type TOKEN
      if (config.policy(req.http.Host, req.url) == 2) {
	 # Handle token authorization:
	 # Assign the non-cryptographic part of the token to a temp
	 # header
	 set req.http.Tmp-Token = config.token();

	 # Use VMOD blobdigest to generate the HMAC, and VMOD blob
	 # to encode the result in lower case hex.
	 # The shared secret serves as the HMAC key, and the token just
	 # assigned to the temp header is the message to be hashed.
	 set req.http.Tmp-HMAC
	   = blob.encode(HEX, LOWER,
	       blobdigest.hmacf(SHA256, config.secret(),
				blob.decode(encoded=
						req.http.Tmp-Token)));

	 # These two temp headers can now be combined to form the full
	 # token string required for authorization at the Akamai
	 # server, such as:
	 #
	 # "hdnea=" + req.http.Tmp-Token + "~hmac=" + req.http.Tmp-HMAC
      }
  }

At minimum, the string returned by ``.token()`` contains the
parameters ``st`` and ``exp``, whose values are the start and end
times (epoch times) for the duration of the authorization. By default,
the authorization begins "now" and lasts for the duration of the TTL
defined in the policy constructor, but these can be overriden by
optional parameters. Other optional parameters can provide values for
additional token parameters such as ``acl`` and ``data``, as described
below.

The ``.secret()`` method returns the BLOB that was provided in the
constructor of the policy object, for the policy that was determined
for the given host and path. Together with VMODs for cryptography,
this can be used to generate the HMAC for authorization. The HMAC and
the string returned from ``.token()`` can then be combined to form the
URL query string or Cookie as required according to Akamai's APIs
(for example, by generating a redirect response from VCL).

Invocations of the ``.token()`` and ``.secret()`` methods have task
scope, meaning that they refer back to the most recent invocation of
``.policy()`` in the same client or backend transaction. For example,
when ``.policy()`` is called in any of the ``vcl_backend_*``
subroutines, subsequent calls to ``.token()`` and ``.secret()`` in the
same backend transaction are based on the policy that was determined
by that call.

INSTALLATION
============

See `INSTALL.rst <INSTALL.rst>`_ in the source repository.

ACKNOWLEDGEMENTS
================

Development of this module was sponsored by BILD GmbH & Co. KG

SUPPORT
=======

.. _gitlab.com issues: https://gitlab.com/uplex/varnish/libvmod-hoailona/-/issues

To report bugs, use `gitlab.com issues`_.

For enquiries about professional service and support, please contact
info@uplex.de\ .

CONTRIBUTING
============

.. _merge requests on gitlab.com: https://gitlab.com/uplex/varnish/libvmod-hoailona/-/merge_requests

To contribute to the project, please use `merge requests on gitlab.com`_.

To support the project's development and maintenance, there are
several options:

.. _paypal: https://www.paypal.com/donate/?hosted_button_id=BTA6YE2H5VSXA

.. _github sponsor: https://github.com/sponsors/nigoroll

* Donate money through `paypal`_. If you wish to receive a commercial
  invoice, please add your details (address, email, any requirements
  on the invoice text) to the message sent with your donation.

* Become a `github sponsor`_.

* Contact info@uplex.de to receive a commercial invoice for SWIFT payment.

SEE ALSO
========

* varnishd(1)
* vcl(7)
* source repository: https://code.uplex.de/uplex-varnish/libvmod-hoailona
* VMOD blobdigest: https://code.uplex.de/uplex-varnish/libvmod-blobdigest

Akamai documentation
--------------------

Technical documentation about SecureHD token authorization appears to
be available only to Akamai customers who have access to the Luna
Control Center. This public document gives a non-technical overview:

* https://www.akamai.com/jp/ja/multimedia/documents/product-brief/securehd-media-content-security-product-brief.pdf

Users of the Luna Control Center can consult:

* SecureHD Policy Editor User's Guide

  * https://control.akamai.com/dl/customers/SPE/spe_ug.pdf

* SecureHD Policy Editor online help

  * https://control.akamai.com/dl/SPE/index.htm

* Sanctioned token generator code (since code is the best documentation)

  * https://control.akamai.com/dl/customers/SPE/EdgeAuth-latest.zip

COPYRIGHT
=========

::

  This document is licensed under the same conditions
  as the libvmod-hoailona project. See LICENSE for details.
 
  Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 
