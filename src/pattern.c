/*-
 * Copyright 2017 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "config.h"

#include <string.h>

#include "vmod_hoailona.h"

static vre_t *chars = NULL, *dots = NULL, *stars = NULL, *meta = NULL;

/*
 * This comparator implements the "specificness" relation between paths
 * assigned to a host according the Akamai SecureHD world. It is used as
 * the comparator for red-black trees assigned to hosts.
 *
 * ass_a < ass_b, and hence path_cmp() returns <0, iff the path assigned
 * in ass_a is less specific than the path assigned in ass_b.
 *
 * Path A is less specific than path B if:
 * A has fewer slashes than B,
 * else if A has '...' and B does not
 * else if A has more *'s than B
 * else if A is shorter than B
 * else if strcmp(A, B) < 0.
 *
 * VRBT_FIND and VRBT_INSERT are only called during vcl_init, so the
 * comparator is not on the critical performance path.
 */
int
path_cmp(const struct assignment * restrict const ass_a,
	 const struct assignment * restrict const ass_b)
{
	char *a = ass_a->pattern->path, *b = ass_b->pattern->path, *p;
	size_t aslashes = 0, bslashes = 0, astars = 0, bstars = 0, alen = 0,
		blen = 0, cmp = 0;
	int adots = 0, bdots = 0;

	p = a;
	while (*p) {
		if (*p == '/')
			aslashes++;
		else if (*p == '*')
			astars++;
		else if (!adots && *p == '.'
			 && (adots = *(p + 1) == '.' && *(p + 2) == '.')) {
			p += 3;
			alen += 3;
			continue;
		}
		alen++;
		p++;
	}
	p = b;
	while (*p) {
		if (cmp == 0 && blen < alen)
			cmp = a[blen] - b[blen];
		if (*p == '/')
			bslashes++;
		else if (*p == '*')
			bstars++;
		else if (!bdots && *p == '.'
			 && (bdots = *(p + 1) == '.' && *(p + 2) == '.')) {
			p += 3;
			blen += 3;
			continue;
		}
		blen++;
		p++;
	}

	if (aslashes != bslashes)
		return bslashes - aslashes;

	if (adots != bdots)
		return adots - bdots;

	if (astars != bstars)
		return astars - bstars;

	if (alen != blen)
		return blen - alen;

	return cmp;
}

VRBT_GENERATE(assign_tree, assignment, entry, path_cmp)

void
validation_init(void)
{
	int err, off;

	chars = VRE_compile(
		"([^A-Za-z0-9 _\\-~.%:/\\[\\]@!$&()*+,;=]+)", 0, &err, &off, 0);
	AN(chars);
	dots = VRE_compile(
		"([^/]\\.\\.\\.[^/]*|[^/]*\\.\\.\\.[^/])", 0, &err, &off, 0);
	AN(dots);
	stars = VRE_compile(
		"(.?\\*{2,}.?)", 0, &err, &off, 0);
	AN(stars);
	meta = VRE_compile(
		"([[\\]$()+])", 0, &err, &off, 0);
	AN(meta);
}

void
validation_fini(void)
{
	VRE_free(&chars);
	AZ(chars);
	VRE_free(&dots);
	AZ(dots);
	VRE_free(&stars);
	AZ(stars);
	VRE_free(&meta);
	AZ(meta);
}

const char *
valid(VRT_CTX, const char * restrict const path)
{
	const char *errmsg = NULL;
	int r;
	txt groups[2];
	size_t len;

	AN(path);
	len = strlen(path);
	memset(groups, 0, sizeof groups);
	r = VRE_capture(chars, path, len, 0, groups, 2, NULL);
	if (r >= 0) {
		if ((errmsg = WS_Printf(ctx->ws,
					"invalid character(s) in pattern: %.*s",
					(int)(groups[1].e - groups[1].b),
					groups[1].b))
		    == NULL)
			return "invalid character(s) in pattern";
		return errmsg;
	}
	memset(groups, 0, sizeof groups);
	r = VRE_capture(dots, path, len, 0, groups, 2, NULL);
	if (r >= 0) {
		if ((errmsg = WS_Printf(ctx->ws,
					"... must only be used before and "
					"after slashes: %.*s",
					(int)(groups[1].e - groups[1].b),
					groups[1].b))
		    == NULL)
			return "... must only be used before and after slashes";
		return errmsg;
	}
	memset(groups, 0, sizeof groups);
	r = VRE_capture(stars, path, len, 0, groups, 2, NULL);
	if (r >= 0) {
		if ((errmsg = WS_Printf(ctx->ws,
					"more than one *: %.*s",
					(int)(groups[1].e - groups[1].b),
					groups[1].b))
		    == NULL)
			return "more than one *";
		return errmsg;
	}
	return NULL;
}

vre_t *
pattern2re(VRT_CTX, const char * restrict const path)
{
	struct vsb *regex;
	const char *esc, *p, *end;
	uintptr_t snap;
	int end_anchor = 1, err, off;
	vre_t *re;

	AN(path);
	snap = WS_Snapshot(ctx->ws);
	esc = VRT_regsub(ctx, 1, path, meta, "\\\1");
	if (WS_Overflowed(ctx->ws)) {
		WS_Reset(ctx->ws, snap);
		return NULL;
	}

	regex = VSB_new_auto();
	CHECK_OBJ_NOTNULL(regex, VSB_MAGIC);

	p = esc;
	end = esc + strlen(esc);
	if (end - esc > 3 && esc[0] == '.' && esc[1] == '.' && esc[2] == '.') {
		VSB_putc(regex, '.');
		p = esc + 3;
	}
	else if (end - esc > 1)
		VSB_putc(regex, '^');
	while (*p) {
		if (*p == '*') {
			VSB_cat(regex, "[^/]+");
			p++;
			continue;
		}
		if (*p == '.') {
			if (end - p >= 3 && p[1] == '.' && p[2] == '.') {
				if (p[3] == '\0') {
					end_anchor = 0;
					break;
				}
				else {
					VSB_cat(regex, ".+");
					p += 3;
					continue;
				}
			}
			else {
				VSB_cat(regex, "\\.");
				p++;
				continue;
			}
		}
		else {
			VSB_putc(regex, *p);
			p++;
		}
	}
	if (end_anchor)
		VSB_putc(regex, '$');
	VSB_finish(regex);

	re = VRE_compile(VSB_data(regex), 0, &err, &off, 0);
	assert(re != NULL);
	VSB_destroy(&regex);
	WS_Reset(ctx->ws, snap);
	return re;
}
