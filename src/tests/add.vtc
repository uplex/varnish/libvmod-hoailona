# looks like -*- vcl -*-

varnishtest "hosts.add()"

varnish v1 -vcl {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new q = hoailona.policy(TOKEN, 2h);
		new r = hoailona.policy(DENY);
		new h = hoailona.hosts();
		h.add("example.com", "q", "/*/...");
		h.add(host="example.org", policy="p");
		h.add("example.com", "r", ".../x/y");
		h.add("varnish-cache.org", "r", "/x/y/./");
	}
} -start

varnish v1 -errvcl {Not available in subroutine 'vcl_recv'} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
	}

	sub vcl_recv {
		h.add("example.com", "p");
	}
}

varnish v1 -errvcl {vmod hoailona error: host is empty in h.add()} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add("", "p", "/foo/bar");
	}
}

varnish v1 -errvcl {vmod hoailona error: policy is empty in h.add()} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add("example.com", "", "/foo/bar");
	}
}

varnish v1 -errvcl {vmod hoailona error: path is set but empty in h.add()} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add("example.com", "p", "");
	}
}

varnish v1 -errvcl {vmod hoailona error: path ""<>?\^`| in h.add(): invalid character(s) in pattern: ""<>?\^`|} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add("example.com", "p", {"""<>?\^`|"});
	}
}

varnish v1 -errvcl {vmod hoailona error: path /x... in h.add(): ... must only be used before and after slashes: x...} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add("example.com", "p", "/x...");
	}
}

varnish v1 -errvcl {vmod hoailona error: path /.../...x in h.add(): ... must only be used before and after slashes: ...x} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add("example.com", "p", "/.../...x");
	}
}

varnish v1 -errvcl {vmod hoailona error: path /x/**/y in h.add(): more than one *: /**/} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add("example.com", "p", "/x/**/y");
	}
}

varnish v1 -errvcl {vmod hoailona error: No policy objects created before calling h.add()} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new h = hoailona.hosts();
		h.add("example.com", "p", "/x/y");
	}
}

varnish v1 -errvcl {vmod hoailona error: Policy object q not found in h.add()} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add("example.com", "q", "/x/y");
	}
}

varnish v1 -errvcl {vmod hoailona error: Policy p already set globally for host example.com in h.add()} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new q = hoailona.policy(TOKEN, 2h);
		new h = hoailona.hosts();
		h.add("example.com", "p");
		h.add("example.com", "q");
	}
}

varnish v1 -errvcl {vmod hoailona error: Path-specific policies already set for host example.com in h.add()} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new q = hoailona.policy(TOKEN, 2h);
		new h = hoailona.hosts();
		h.add("example.com", "p", "/foo/bar");
		h.add("example.com", "q");
	}
}

varnish v1 -errvcl {vmod hoailona error: Policy p already assigned for host example.com and path /foo/bar in h.add()} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new q = hoailona.policy(TOKEN, 2h);
		new h = hoailona.hosts();
		h.add("example.com", "p", "/foo/bar");
		h.add("example.com", "q", "/foo/bar");
	}
}

# The following assignments are legal since the paths don't compare as
# equal.
varnish v1 -vcl {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new q = hoailona.policy(TOKEN, 2h);
		new h = hoailona.hosts();
		h.add("example.com", "p", "/foo/bar");
		h.add("example.com", "q", "/bar/foo");
	}
}

varnish v1 -vcl {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new q = hoailona.policy(TOKEN, 2h);
		new h = hoailona.hosts();
		h.add("example.com", "p", "/foo/bar");
		h.add("example.com", "q", "/foo/bar/baz");
	}
}

varnish v1 -vcl {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN, 1h);
		new q = hoailona.policy(TOKEN, 2h);
		new h = hoailona.hosts();
		h.add("example.com", "p", "/foo/bar");
		h.add("example.com", "q", "/foo/b/r");
	}
}

varnish v1 -vcl {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new q = hoailona.policy(TOKEN, 2h);
		new h = hoailona.hosts();
		h.add("example.com", "p", "/foo/bar");
		h.add("example.com", "q", "/foo/...");
	}
}

varnish v1 -vcl {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new q = hoailona.policy(TOKEN, 2h);
		new h = hoailona.hosts();
		h.add("example.com", "p", ".../bar");
		h.add("example.com", "q", "/foo/bar");
	}
}

varnish v1 -vcl {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new q = hoailona.policy(TOKEN, 2h);
		new h = hoailona.hosts();
		h.add("example.com", "p", "/foo/bar");
		h.add("example.com", "q", "/foo/*");
	}
}

varnish v1 -vcl {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new q = hoailona.policy(TOKEN, 2h);
		new h = hoailona.hosts();
		h.add("example.com", "p", "/foo/bar");
		h.add("example.com", "q", "/foo/barz");
	}
}

# Test valid hostnames
varnish v1 -vcl {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add("*example.com", "p");
		h.add("EXAMPLE-EXAMPLE.EXAMPLE.COM", "p");
		h.add("abcdefg-hijklmnop.qrstuv-wxyz.ABCDEFG-HIJKLMNOP.QRSTUV-WXYZ.0123-456.789", "p");
	}
}

varnish v1 -errvcl {invalid hostname -example.com: may not begin with - or .} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add("-example.com", "p");
	}
}

varnish v1 -errvcl {invalid hostname .example.com: may not begin with - or .} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add(".example.com", "p");
	}
}

varnish v1 -errvcl {invalid hostname *.*.example.com: illegal characters} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add("*.*.example.com", "p");
	}
}

varnish v1 -errvcl {invalid hostname example-%.com: illegal characters} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add("example-%.com", "p");
	}
}

varnish v1 -errvcl {invalid hostname example-ä.com: illegal characters} {
	import hoailona;
	backend proforma none;

	sub vcl_init {
		new p = hoailona.policy(OPEN);
		new h = hoailona.hosts();
		h.add("example-ä.com", "p");
	}
}
