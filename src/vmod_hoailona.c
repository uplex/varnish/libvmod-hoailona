/*-
 * Copyright 2017 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "vmod_hoailona.h"
#include "vas.h"
#include "vcc_hoailona_if.h"

#define epfx "vmod hoailona error: "

#define ERR(ctx, msg) do {						\
		if (ctx->method & VCL_MET_TASK_H)			\
			VRT_fail(ctx, epfx msg);			\
		else							\
			VSLb(ctx->vsl, SLT_VCL_Error, epfx msg);	\
	} while(0)

#define VERR(ctx, fmt, ...) do {					\
		if (ctx->method & VCL_MET_TASK_H)			\
			VRT_fail(ctx, epfx fmt, __VA_ARGS__);		\
		else							\
			VSLb(ctx->vsl, SLT_VCL_Error, epfx fmt, __VA_ARGS__); \
	} while(0)

#define VERRNOMEM(ctx, fmt, ...) \
        VERR((ctx), fmt ", out of space", __VA_ARGS__)

#define ERRNOMEM(ctx, msg) \
        ERR((ctx), msg ", out of space")

struct host {
	unsigned			magic;
#define VMOD_HOAILONA_HOST_MAGIC 0x731af58f
	struct assign_tree		assignments;
	VSTAILQ_ENTRY(host)		list;
	char				*name;
	char				*description;
	struct vmod_hoailona_policy	*policy;
	size_t				len;
};

typedef VSTAILQ_HEAD(hosthead, host) hosthead_t;

struct vmod_hoailona_hosts {
	unsigned	magic;
#define VMOD_HOAILONA_HOSTS_MAGIC 0xa3ef1ea9
	hosthead_t	hosthead;
	char		*vcl_name;
};

struct policyitem {
	VSLIST_ENTRY(policyitem)	list;
	struct vmod_hoailona_policy	*policy;
};

typedef VSLIST_HEAD(policyhead, policyitem) policyhead_t;

struct policy_task {
	unsigned			magic;
#define VMOD_HOAILONA_POLICY_TASK_MAGIC 0x5fc90249
	struct host			*host;
	struct assignment		*assignment;
	struct vmod_hoailona_policy	*policy;
};

#define BLOB_VMOD_HOAILONA_SECRET_TYPE	0xaa50e92c

static inline void
WS_Contains(struct ws * const restrict ws, const void * const restrict ptr,
            const size_t len)
{
        assert((char *)ptr >= ws->s && (char *)(ptr + len) <= ws->e);
}

static struct vmod_hoailona_policy *
get_policy(VRT_CTX, const struct vmod_priv * restrict const,
	   const char * restrict const,
	   const char * restrict const);

/* Event function */

static int loadcnt = 0;
int v_matchproto_(vmod_event_f)
vmod_event(VRT_CTX, struct vmod_priv *priv, enum vcl_event_e e)
{
	(void) ctx;
	(void) priv;

	switch(e) {
	case VCL_EVENT_LOAD:
		if (loadcnt++ == 0)
			validation_init();
		return (0);
	case VCL_EVENT_DISCARD:
		if (--loadcnt == 0)
			validation_fini();
		return (0);
	default:
		return (0);
	}
}

/* Object policy */

VCL_VOID
vmod_policy__init(VRT_CTX, struct vmod_hoailona_policy **policyp,
		  const char *vcl_name, struct vmod_priv *init_task,
		  VCL_ENUM policys, VCL_DURATION ttl, VCL_STRING description,
		  VCL_BLOB secret_in, VCL_INT start_offset)
{
	struct vmod_hoailona_policy *policy;
	policyhead_t *policyhead;
	struct policyitem *item;
	struct vrt_blob *secret;
	unsigned char *spc;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ctx->ws, WS_MAGIC);
	AN(policyp);
	AZ(*policyp);
	AN(vcl_name);
	AN(policys);
	ALLOC_OBJ(policy, VMOD_HOAILONA_POLICY_MAGIC);
	AN(policy);
	*policyp = policy;

	if (init_task->priv == NULL) {
		if ((policyhead = WS_Alloc(ctx->ws, sizeof(policyhead_t)))
		    == NULL) {
			VERRNOMEM(ctx,
				  "initializing policy list in %s constructor",
				  vcl_name);
			return;
		}
		VSLIST_INIT(policyhead);
		init_task->priv = policyhead;
	}
	else {
		WS_Contains(ctx->ws, init_task->priv, sizeof(policyhead_t));
		policyhead = init_task->priv;
	}
	if ((item = WS_Alloc(ctx->ws, sizeof(struct policyitem))) == NULL) {
		VERRNOMEM(ctx,
			  "allocating policy list item in %s constructor",
			  vcl_name);
		return;
	}

	if (strcmp(policys, "DENY") == 0)
		policy->type = DENY;
	else if (strcmp(policys, "OPEN") == 0)
		policy->type = OPEN;
	else if (strcmp(policys, "TOKEN") == 0)
		policy->type = TOKEN;
	else
		WRONG("illegal policy enum");
	if (policy->type == TOKEN && ttl <= 0.) {
		VERR(ctx, "ttl must be >= 0 when type is TOKEN "
		     "in %s constructor", vcl_name);
		return;
	}

	item->policy = policy;
	VSLIST_INSERT_HEAD(policyhead, item, list);

	policy->vcl_name = strdup(vcl_name);
	AN(policy->vcl_name);
	if (description != NULL)
		policy->description = strdup(description);
	else
		AZ(policy->description);
	if (secret_in != NULL && secret_in->len > 0) {
		AN(secret_in->blob);
		spc = malloc(sizeof(*secret) + secret_in->len);
		AN(spc);

		secret = (void *)spc;
		spc += sizeof(*secret);

		memcpy(spc, secret_in->blob, secret_in->len);
		secret->blob = spc;
		secret->len = secret_in->len;
		secret->type = BLOB_VMOD_HOAILONA_SECRET_TYPE;

		policy->secret = secret;
	}
	else
		AZ(policy->secret);
	policy->ttl = ttl;
	policy->start_offset = start_offset;
}

VCL_VOID
vmod_policy__fini(struct vmod_hoailona_policy **policyp)
{
	struct vmod_hoailona_policy *policy;

	policy = *policyp;
	if (policy == NULL)
		return;
	*policyp = NULL;
	CHECK_OBJ_NOTNULL(policy, VMOD_HOAILONA_POLICY_MAGIC);
	if (policy->vcl_name != NULL)
		free(policy->vcl_name);
	if (policy->description != NULL)
		free(policy->description);
	if (policy->secret != NULL) {
		// single allocation including blob
		free(policy->freeptr);
	}
	FREE_OBJ(policy);
}

/* Object hosts */

VCL_VOID
vmod_hosts__init(VRT_CTX, struct vmod_hoailona_hosts **hostsp,
		 const char *vcl_name)
{
	struct vmod_hoailona_hosts *hosts;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	AN(hostsp);
	AZ(*hostsp);
	AN(vcl_name);
	ALLOC_OBJ(hosts, VMOD_HOAILONA_HOSTS_MAGIC);
	AN(hosts);
	*hostsp = hosts;

	hosts->vcl_name = strdup(vcl_name);
	AN(hosts->vcl_name);
	VSTAILQ_INIT(&hosts->hosthead);
}

/*
 * The hosts finalizer does not clean up policy objects that the hosts
 * object might point to internally, varnishd will take care of that by
 * running the policy finalizer.
 */
VCL_VOID
vmod_hosts__fini(struct vmod_hoailona_hosts **hostsp)
{
	struct vmod_hoailona_hosts *hosts;
	struct host *host = NULL;

	hosts = *hostsp;
	if (hosts == NULL)
		return;
	*hostsp = NULL;
	CHECK_OBJ_NOTNULL(hosts, VMOD_HOAILONA_HOSTS_MAGIC);
	if (hosts->vcl_name != NULL)
		free(hosts->vcl_name);
	host = VSTAILQ_FIRST(&hosts->hosthead);
	while (host != NULL) {
		struct assignment *a;
		struct host *next_host = NULL;

		CHECK_OBJ_NOTNULL(host, VMOD_HOAILONA_HOST_MAGIC);
		AN(host->name);

		free(host->name);
		if (host->description != NULL)
			free(host->description);
		a = VRBT_MIN(assign_tree, &host->assignments);
		while (a != NULL) {
			struct assignment *next_ass;

			CHECK_OBJ_NOTNULL(a, VMOD_HOAILONA_ASSIGNMENT_MAGIC);
			if (a->description != NULL)
				free(a->description);
			if (a->pattern != NULL) {
				CHECK_OBJ(a->pattern,
					  VMOD_HOAILONA_PATTERN_MAGIC);
				if (a->pattern->path != NULL)
					free(a->pattern->path);
				if (a->pattern->re != NULL)
					VRE_free(&a->pattern->re);
			}
			next_ass = VRBT_NEXT(assign_tree, a, a);
			FREE_OBJ(a);
			a = next_ass;
		}
		next_host = VSTAILQ_NEXT(host, list);
		FREE_OBJ(host);
		host = next_host;
	}
	FREE_OBJ(hosts);
}

static inline int
valid_hostname(VRT_CTX, const char * restrict const hostname)
{
	const char *p = hostname;

	if (*p == '-' || *p == '.') {
		VERR(ctx, "invalid hostname %s: may not begin with - or .",
		     hostname);
		return 0;
	}
	else if (*p == '*')
		p++;
	while (*p) {
		if (!isalnum(*p) && *p != '-' && *p != '.') {
			VERR(ctx, "invalid hostname %s: illegal characters",
			     hostname);
			return 0;
		}
		p++;
	}
	return 1;
}

VCL_VOID
vmod_hosts_add(VRT_CTX, struct vmod_hoailona_hosts *hosts,
	       struct vmod_priv *init_task, VCL_STRING hostname,
	       VCL_STRING policyname, VCL_STRING path, VCL_STRING description)
{
	policyhead_t *policyhead;
	struct policyitem *item;
	struct vmod_hoailona_policy *policy = NULL;
	struct host *host = NULL;
	struct pattern *pattern;
	struct assignment *assign;
	vre_t *re = NULL;
	const char *err = NULL;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ctx->ws, WS_MAGIC);
	CHECK_OBJ_NOTNULL(hosts, VMOD_HOAILONA_HOSTS_MAGIC);
	AN(init_task);
	assert(ctx->method == VCL_MET_INIT);

	if (hostname == NULL || hostname[0] == '\0') {
		VERR(ctx, "host is empty in %s.add()", hosts->vcl_name);
		return;
	}
	if (!valid_hostname(ctx, hostname))
		return;
	if (policyname == NULL || policyname[0] == '\0') {
		VERR(ctx, "policy is empty in %s.add()", hosts->vcl_name);
		return;
	}
	if (path != NULL) {
		if (path[0] == '\0') {
			VERR(ctx, "path is set but empty in %s.add()",
			     hosts->vcl_name);
			return;
		}
		if ((err = valid(ctx, path)) != NULL) {
			VERR(ctx, "path %s in %s.add(): %s", path,
			     hosts->vcl_name, err);
			return;
		}
		if ((re = pattern2re(ctx, path)) == NULL) {
			VERRNOMEM(ctx, "converting path %s to regex in %s.add()",
				  path, hosts->vcl_name);
			return;
		}
	}

	if (init_task->priv == NULL) {
		VERR(ctx, "No policy objects created before calling %s.add()",
		     hosts->vcl_name);
		return;
	}
	WS_Contains(ctx->ws, init_task->priv, sizeof(policyhead_t));
	policyhead = init_task->priv;
	VSLIST_FOREACH(item, policyhead, list) {
		CHECK_OBJ_NOTNULL(item->policy, VMOD_HOAILONA_POLICY_MAGIC);
		if (strcmp(policyname, item->policy->vcl_name) == 0) {
			policy = item->policy;
			break;
		}
	}
	if (policy == NULL) {
		VERR(ctx, "Policy object %s not found in %s.add()", policyname,
		     hosts->vcl_name);
		return;
	}

	VSTAILQ_FOREACH(host, &hosts->hosthead, list) {
		CHECK_OBJ(host, VMOD_HOAILONA_HOST_MAGIC);
		if (strcmp(hostname, host->name) == 0)
			break;
	}
	if (host != NULL) {
		if (path == NULL && host->policy != NULL) {
			CHECK_OBJ(host->policy, VMOD_HOAILONA_POLICY_MAGIC);
			AZ(re);
			VERR(ctx, "Policy %s already set globally for host %s "
			     "in %s.add()", host->policy->vcl_name, hostname,
			     hosts->vcl_name);
			return;
		}
		else if (path == NULL && !VRBT_EMPTY(&host->assignments)) {
			VERR(ctx, "Path-specific policies already set for "
			     "host %s in %s.add()", hostname, hosts->vcl_name);
			AZ(re);
			return;
		}
		else if (path != NULL && !VRBT_EMPTY(&host->assignments)) {
			struct pattern tmp_pattern;
			struct assignment tmp_assign, *result;

			tmp_pattern.path = (void *)path;
			tmp_assign.pattern = &tmp_pattern;
			if ((result = VRBT_FIND(assign_tree, &host->assignments,
					       &tmp_assign))
			    != NULL) {
				VERR(ctx, "Policy %s already assigned for host "
				     "%s and path %s in %s.add()",
				     result->policy->vcl_name, hostname, path,
				     hosts->vcl_name);
				if (re != NULL)
					VRE_free(&re);
				return;
			}
		}
	}
	else {
		ALLOC_OBJ(host, VMOD_HOAILONA_HOST_MAGIC);
		AN(host);
		host->name = strdup(hostname);
		host->len = strlen(hostname);
		VRBT_INIT(&host->assignments);
		AZ(host->description);
		AZ(host->policy);
		VSTAILQ_INSERT_TAIL(&hosts->hosthead, host, list);
	}

	if (path == NULL) {
		host->policy = policy;
		if (description != NULL)
			host->description = strdup(description);
		return;
	}

	ALLOC_OBJ(pattern, VMOD_HOAILONA_PATTERN_MAGIC);
	AN(pattern);
	AN(re);
	pattern->re = re;
	pattern->path = strdup(path);

	ALLOC_OBJ(assign, VMOD_HOAILONA_ASSIGNMENT_MAGIC);
	AN(assign);
	assign->policy = policy;
	assign->pattern = pattern;
	if (description != NULL)
		assign->description = strdup(description);
	AZ(VRBT_INSERT(assign_tree, &host->assignments, assign));
}

VCL_INT
vmod_hosts_policy(VRT_CTX, struct vmod_hoailona_hosts *hosts,
		  struct vmod_priv *priv_task, VCL_STRING hostname,
		  VCL_STRING pathname)
{
	struct policy_task *task;
	struct host *h, *host = NULL;
	struct vmod_hoailona_policy *policy;
	size_t hostlen;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ctx->ws, WS_MAGIC);
	CHECK_OBJ_NOTNULL(hosts, VMOD_HOAILONA_HOSTS_MAGIC);
	AN(priv_task);
	AZ(ctx->method & VCL_MET_INIT);
	if (hostname == NULL || hostname[0] == '\0') {
		if (! (pathname == NULL || pathname[0] == '\0')) {
			VERR(ctx, "host is empty in %s.policy()",
			     hosts->vcl_name);
			return -2;
		}

		if (priv_task->priv == NULL) {
			VERR(ctx, "%s.policy() no cached result",
			     hosts->vcl_name);
			return -2;
		}

		policy = get_policy(ctx, priv_task, hosts->vcl_name, "policy");

		if (policy == NULL)
			return -1;
		return policy->type;
	} else if (pathname == NULL || pathname[0] == '\0') {
		VERR(ctx, "path is empty in %s.policy()", hosts->vcl_name);
		return -2;
	}

	if (priv_task->priv == NULL) {
		if ((task = WS_Alloc(ctx->ws, sizeof(struct policy_task)))
		    == NULL) {
			VERRNOMEM(ctx, "allocating task data in %s.policy()",
				  hosts->vcl_name);
			return -2;
		}
		task->magic = VMOD_HOAILONA_POLICY_TASK_MAGIC;
		priv_task->priv = task;
		AZ(priv_task->methods);
	}
	else {
		WS_Contains(ctx->ws, priv_task->priv,
			    sizeof(struct policy_task));
		CAST_OBJ(task, priv_task->priv,
			 VMOD_HOAILONA_POLICY_TASK_MAGIC);
	}
	task->host = NULL;
	task->assignment = NULL;
	task->policy = NULL;

	/* XXX optimize */
	hostlen = strlen(hostname);
	VSTAILQ_FOREACH(h, &hosts->hosthead, list) {
		const char *q, *hs;

		CHECK_OBJ(h, VMOD_HOAILONA_HOST_MAGIC);
		AN(h->name);
		if (*h->name == '*') {
			if (h->len - 1 > hostlen)
				continue;
			hs = h->name + 1;
			q = hostname + (hostlen - h->len + 1);
		}
		else {
			if (h->len != hostlen)
				continue;
			hs = h->name;
			q = hostname;
		}
		if (strcmp(q, hs) == 0) {
			host = h;
			break;
		}
	}
	if (host == NULL)
		return -1;

	/* There is a policy XOR there is a list of assignments */
	assert((host->policy != NULL && VRBT_EMPTY(&host->assignments))
	       || (host->policy == NULL && !VRBT_EMPTY(&host->assignments)));
	if (host->policy != NULL) {
		task->host = host;
		task->policy = host->policy;
		policy = host->policy;
		AZ(task->assignment);
	}
	else {
		struct assignment *a, *assignment = NULL;
		int pathlen = strlen(pathname);

		/* XXX lazily compute and save VRBT_MIN for the host
		   assignments, and re-use it to init the for loop */
		VRBT_FOREACH(a, assign_tree, &host->assignments) {
			int match;

			CHECK_OBJ(a, VMOD_HOAILONA_ASSIGNMENT_MAGIC);
			CHECK_OBJ_NOTNULL(a->pattern,
					  VMOD_HOAILONA_PATTERN_MAGIC);
			AN(a->pattern->re);

			match = VRE_match(a->pattern->re, pathname, pathlen, 0, NULL);
			if (match >= 0) {
				assignment = a;
				break;
			}
			assert(match == VRE_ERROR_NOMATCH);
		}
		if (assignment == NULL)
			return -1;

		CHECK_OBJ_NOTNULL(assignment->policy,
				  VMOD_HOAILONA_POLICY_MAGIC);
		task->host = host;
		task->assignment = assignment;
		policy = assignment->policy;
		AZ(task->policy);
	}

	CHECK_OBJ_NOTNULL(policy, VMOD_HOAILONA_POLICY_MAGIC);
	return policy->type;
}

static struct vmod_hoailona_policy *
get_policy(VRT_CTX, const struct vmod_priv * restrict const priv_task,
	   const char * restrict const vcl_name,
	   const char * restrict const method)
{
	struct policy_task *task;

	AN(priv_task);
	CHECK_OBJ_NOTNULL(ctx->ws, WS_MAGIC);
	if (priv_task->priv == NULL) {
		VERR(ctx, "%s.%s() called before %s.policy()", vcl_name, method,
		     vcl_name);
		return NULL;
	}
	WS_Contains(ctx->ws, priv_task->priv, sizeof(struct policy_task));
	CAST_OBJ(task, priv_task->priv, VMOD_HOAILONA_POLICY_TASK_MAGIC);

	if (task->policy != NULL) {
		CHECK_OBJ(task->policy, VMOD_HOAILONA_POLICY_MAGIC);
		return task->policy;
	}

	if (task->assignment == NULL) {
		VERR(ctx, "%s.%s() no policy", vcl_name, method);
		return NULL;
	}

	CHECK_OBJ_NOTNULL(task->assignment, VMOD_HOAILONA_ASSIGNMENT_MAGIC);
	CHECK_OBJ_NOTNULL(task->assignment->policy, VMOD_HOAILONA_POLICY_MAGIC);
	return task->assignment->policy;
}

VCL_STRING
vmod_hosts_token(VRT_CTX, struct vmod_hoailona_hosts *hosts,
		 struct vmod_priv *priv_task, VCL_STRING acl, VCL_DURATION ttl,
		 VCL_STRING data)
{
	struct vmod_hoailona_policy *policy;
	VCL_DURATION t;
	int st, exp;
	VCL_STRING token;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ctx->ws, WS_MAGIC);
	CHECK_OBJ_NOTNULL(hosts, VMOD_HOAILONA_HOSTS_MAGIC);
	AZ(ctx->method & VCL_MET_INIT);
	if (ttl < 0) {
		VERR(ctx, "ttl must not be < 0 in %s.token(): %f",
		     hosts->vcl_name, ttl);
		return NULL;
	}

	policy = get_policy(ctx, priv_task, hosts->vcl_name, "token");
	if (policy == NULL)
		return NULL;
	if (policy->type != TOKEN) {
		VERR(ctx, "in %s.token(): policy %s does not specify a token",
		     hosts->vcl_name, policy->vcl_name);
		return NULL;
	}

	t = policy->ttl;
	if (ttl > 0)
		t = ttl;
	st = (int) (ctx->now + policy->start_offset);
	exp = (int) (ctx->now + t); // XXX should it be st + t ?
	if (acl != NULL) {
		if (data != NULL)
			token = WS_Printf(ctx->ws,
					  "st=%d~exp=%d~acl=%s~data=%s",
					  st, exp, acl, data);
		else
			token = WS_Printf(ctx->ws, "st=%d~exp=%d~acl=%s",
					  st, exp, acl);
	}
	else if (data != NULL) {
		token = WS_Printf(ctx->ws, "st=%d~exp=%d~data=%s",
				  st, exp, data);
	}
	else
		token = WS_Printf(ctx->ws, "st=%d~exp=%d", st, exp);

	if (token == NULL) {
		VERRNOMEM(ctx, "in %s.token()", hosts->vcl_name);
		return NULL;
	}
	return token;
}

VCL_BLOB
vmod_hosts_secret(VRT_CTX, struct vmod_hoailona_hosts *hosts,
		  struct vmod_priv *priv_task)
{
	struct vmod_hoailona_policy *policy;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(hosts, VMOD_HOAILONA_HOSTS_MAGIC);
	AZ(ctx->method & VCL_MET_INIT);

	policy = get_policy(ctx, priv_task, hosts->vcl_name, "secret");
	if (policy == NULL)
		return NULL;
	return policy->secret;
}

VCL_STRING
vmod_hosts_explain(VRT_CTX, struct vmod_hoailona_hosts *hosts,
		   struct vmod_priv *priv_task)
{
	struct policy_task *task;
	struct host *host;
	struct vmod_hoailona_policy *policy;
	VCL_STRING ret = NULL;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(ctx->ws, WS_MAGIC);
	CHECK_OBJ_NOTNULL(hosts, VMOD_HOAILONA_HOSTS_MAGIC);
	AN(priv_task);
	AZ(ctx->method & VCL_MET_INIT);
	if (priv_task->priv == NULL) {
		VERR(ctx, "%s.explain() called before %s.policy()",
		     hosts->vcl_name, hosts->vcl_name);
		return NULL;
	}
	WS_Contains(ctx->ws, priv_task->priv, sizeof(struct policy_task));
	CAST_OBJ(task, priv_task->priv, VMOD_HOAILONA_POLICY_TASK_MAGIC);
	host = task->host;

	if (task->policy != NULL) {
		CHECK_OBJ_NOTNULL(host, VMOD_HOAILONA_HOST_MAGIC);
		AN(host->name);
		CHECK_OBJ(task->policy, VMOD_HOAILONA_POLICY_MAGIC);
		AN(task->policy->vcl_name);
		policy = task->policy;
		if (policy->description != NULL) {
			if (host->description != NULL)
				ret = WS_Printf(ctx->ws, "Matched host %s (%s) "
						"for global policy %s (%s)",
						host->name, host->description,
						policy->vcl_name,
						policy->description);
			else
				ret = WS_Printf(ctx->ws, "Matched host %s "
						"for global policy %s (%s)",
						host->name, policy->vcl_name,
						policy->description);
		}
		else {
			if (host->description != NULL)
				ret = WS_Printf(ctx->ws, "Matched host %s (%s) "
						"for global policy %s",
						host->name, host->description,
						policy->vcl_name);
			else
				ret = WS_Printf(ctx->ws, "Matched host %s "
						"for global policy %s",
						host->name, policy->vcl_name);
		}
	}
	else if (task->assignment != NULL) {
		struct assignment *a;

		CHECK_OBJ_NOTNULL(host, VMOD_HOAILONA_HOST_MAGIC);
		AN(host->name);
		CHECK_OBJ_NOTNULL(task->assignment,
				  VMOD_HOAILONA_ASSIGNMENT_MAGIC);
		CHECK_OBJ_NOTNULL(task->assignment->policy,
				  VMOD_HOAILONA_POLICY_MAGIC);
		CHECK_OBJ_NOTNULL(task->assignment->pattern,
				  VMOD_HOAILONA_PATTERN_MAGIC);
		AN(task->assignment->policy->vcl_name);
		AN(task->assignment->pattern->path);
		AZ(host->description);
		a = task->assignment;
		policy = a->policy;
		if (policy->description != NULL) {
			if (a->description != NULL)
				ret = WS_Printf(ctx->ws,
						"Matched host %s "
						"and pattern %s (%s) "
						"for policy %s (%s)",
						host->name, a->pattern->path,
						a->description,
						policy->vcl_name,
						policy->description);
			else
				ret = WS_Printf(ctx->ws,
						"Matched host %s "
						"and pattern %s "
						"for policy %s (%s)",
						host->name, a->pattern->path,
						policy->vcl_name,
						policy->description);
		}
		else {
			if (a->description != NULL)
				ret = WS_Printf(ctx->ws,
						"Matched host %s "
						"and pattern %s (%s) "
						"for policy %s",
						host->name, a->pattern->path,
						a->description,
						policy->vcl_name);
			else
				ret = WS_Printf(ctx->ws,
						"Matched host %s "
						"and pattern %s "
						"for policy %s",
						host->name, a->pattern->path,
						policy->vcl_name);
		}
	}
	else
		ret = WS_Printf(ctx->ws, "%s", "No policy was matched");

	if (ret == NULL)
		VERRNOMEM(ctx, "in %s.explain()", hosts->vcl_name);
	return ret;
}

/* Functions */

VCL_STRING
vmod_version(VRT_CTX __attribute__((unused)))
{
	return VERSION;
}
