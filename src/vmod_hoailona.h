/*-
 * Copyright 2017 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "cache/cache.h"
#include "vre.h"
#include "vtree.h"
#include "vcl.h"
#include "vsb.h"

enum policy_type {
   DENY = 0,
   OPEN = 1,
   TOKEN = 2
};

struct pattern {
	unsigned	magic;
#define VMOD_HOAILONA_PATTERN_MAGIC 0x1876e01f
	char		*path;
	vre_t		*re;
};

struct vmod_hoailona_policy {
	unsigned		magic;
#define VMOD_HOAILONA_POLICY_MAGIC 0xf729cbfa
	char			*vcl_name;
	char			*description;
	union {
		VCL_BLOB	secret;
		void		*freeptr;
	};
	VCL_DURATION		ttl;
	enum policy_type	type;
	VCL_INT			start_offset;
};

struct assignment {
	unsigned			magic;
#define VMOD_HOAILONA_ASSIGNMENT_MAGIC 0x7523d6e8
	VRBT_ENTRY(assignment)		entry;
	struct pattern			*pattern;
	struct vmod_hoailona_policy	*policy;
	char				*description;
};

VRBT_HEAD(assign_tree, assignment);

int path_cmp(const struct assignment * restrict const ass_a,
	     const struct assignment * restrict const ass_b);

VRBT_PROTOTYPE(assign_tree, assignment, entry, path_cmp)

void validation_init(void);
void validation_fini(void);

const char * valid(VRT_CTX, const char * restrict const path);
vre_t * pattern2re(VRT_CTX, const char * restrict const path);
